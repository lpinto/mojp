<?php  
	require_once 'header.php'; 
	require_once 'inc/manager-db.php';

    if (!isset($_GET['page']))
        $_GET['page'] = 1;
?>

<main role="main" >
    <?php $customerInformations =customerInformations($_GET);
        $nbPages = nbPages();
    ?>

    <h1 align="center"><b><i> Orders </i></b></h1>
    <br>
    <table class="table table-bordered table-striped table-sm">
        <thead>
        <tr align="center" >
            <th> id</th>
            <th> Mail </th>
            <th>Last Name</th>
            <th>First Name</th>
            <th>Address</th>
            <th>D/O</th>
            <th>D/S</th>
            <th>Item</th>
            <th>Ship</th>
            <th>RR JP</th>
            <th>Order#</th>
            <th>Note</th>
            <th>Actions</th>
        </tr>
        </thead>

        <tbody>
            <?php foreach ($customerInformations as $information): 
                $informationsMojp = informationsMojp($information->id_order); ?>
                <tr align="center">
                    <td> <?php echo $information->id_order; ?> </td>
                    <td> 
                        <?php if (endsWith($information->email, "marketplace.amazon.co.uk")){
                            echo 'AZ';
                        }
                        else
                            echo $information->email; ?>  
                    </td>
                    <td><?php echo $information->lastname; ?></td>
                    <td><?php echo $information->firstname; ?></td>
                    <td><?php echo $information->address1; ?></td>
                    <td><?php echo $information->date_add; ?></td>
                    <td><?php foreach ($informationsMojp as $key) {
                                if (!empty($key->shipping_date))
                                    echo $key->shipping_date;
                            } ?></td>
                    <td>
                        <?php $itemsInformations = itemInformations($information->id_order);
                            foreach ($itemsInformations as $informationItems):
                                echo $informationItems->product_quantity; ?>x
                                <?php echo "$informationItems->product_name $informationItems->reference";
                            ?> <br>
                        <?php endforeach;?>
                    </td>
                    <td><?php echo $information->name; ?></td>
                    <td> <?php foreach ($informationsMojp as $key) {
                                    if (!empty ($key->tracking_number))
                                        echo $key->tracking_number;
                                } ?></td>
                    <td><?php echo $information->reference; ?></td>
                    <td> <a  data-toggle="modal" data-target="#notes"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a> 
                        <!-- Modal -->
                        <div class="modal fade" id="notes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h4 align="center" class="modal-title" id="exampleModalLabel" >Notes</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <?php foreach ($informationsMojp as $key) {
                                        echo $key->notes;
                                } ?>
                              </div>
                            </div>
                          </div>
                        </div></td>
                    <td>
                        <a href="edit.php?id=<?php echo $information->id_order; ?>" ><button class="btn btn-primary">Edit</button></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-end">
            
            <?php if ($_GET['page'] == 1): ?>
                <li class="page-item disabled">

            <?php else: ?>
                <li class="page-item ">
            <?php endif; ?>
            <a class="page-link" href="index.php?page=<?php echo $_GET['page']-1; ?>" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span> </a> </li>

            <?php for($i = 1; $i <= $nbPages; $i++): ?>
                <?php if($_GET['page'] == $i): ?>
                        <li class="page-item active">
                <?php endif;?>
                <a class="page-link" href="index.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>

            <?php endfor; ?>
            
            <?php if ($_GET['page'] != $nbPages): ?>
                <li class="page-item">
            <?php else: ?>
                <li class="page-item disabled">
            <?php endif; ?>

            <a class="page-link" href="index.php?page=<?php echo $_GET['page']+1; ?>" aria-label="Previous">
                <span aria-hidden="true">&raquo;</span> </a> </li>
        </ul>
    </nav>
</main>

<?php
require_once 'javascripts.php';
require_once 'footer.php';
?>
