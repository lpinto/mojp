<?php
require_once 'connect-db.php';

function nbPages(){
    global $pdo_ps;

    $sql = 'SELECT COUNT(id_order) as nbOrders FROM ps_orders ';

    $data = $pdo_ps->query($sql)->fetch();
    $nbOrders = $data->nbOrders;
    $nbPages = ceil($nbOrders / 5);
    return $nbPages;
}

function customerInformations($parametre){
    global $pdo_ps;

    if (isset($parametre['page'])){
        $pageAct = $parametre['page'];
    }
    else{
        $pageAct = 1;
    }
    $parPage = 5;

    $query = "SELECT DISTINCT id_order,email, ps_customer.lastname,ps_customer.firstname, address1,
              ps_orders.date_add, ps_carrier.name, reference
              FROM ps_customer, ps_address, ps_orders, ps_carrier
              WHERE ps_customer.id_customer = ps_address.id_customer
              AND ps_customer.id_customer = ps_orders.id_customer
              AND ps_carrier.id_carrier = ps_orders.id_carrier ORDER by id_order DESC LIMIT :debut,:limit";
    $prep = $pdo_ps->prepare($query);
    $prep->bindValue(':debut', (($pageAct-1)*$parPage),PDO::PARAM_INT);
    $prep->bindValue(':limit', $parPage, PDO::PARAM_INT);
    $prep->execute();
    return $prep->fetchAll();
}

function itemInformations($id){
    global $pdo_ps;

    $query="SELECT  product_name, product_quantity, reference 
            FROM `ps_order_detail`, ps_product
            WHERE ps_product.id_product = ps_order_detail.product_id
            AND id_order = $id ";
    return $pdo_ps->query($query)->fetchAll();
}

function addMOJP($parametre){
    global $pdo_mojp;

    if ($parametre['info'] == "notes") {
        $sql = "INSERT INTO ldb_orders(notes, id_presta_order) VALUES (:notes, :id)";
        $prep = $pdo_mojp->prepare($sql);
        $prep->bindValue(':notes', $parametre['notes'], PDO::PARAM_STR);
        $prep->bindValue(':id', $parametre['idOrder'], PDO::PARAM_INT);
        $prep->execute();
    }
    else{
        $sql = "INSERT INTO ldb_orders(status, id_presta_order) VALUES (:status, :id)";
        $prep = $pdo_mojp->prepare($sql);
        $prep->bindValue(':status', $parametre['status'], PDO::PARAM_INT);
        $prep->bindValue(':id', $parametre['idOrder'], PDO::PARAM_INT);
        $prep->execute();
    
    }
}

function updateMOJP($parametre){
    global $pdo_mojp;

    if ($parametre['info'] == "notes") {
        $sql = "UPDATE ldb_orders SET notes = :notes WHERE id_presta_order = :idOrder";
        $prep = $pdo_mojp->prepare($sql);
        $prep->bindValue(':notes', $parametre['notes']);
        $prep->bindValue(':idOrder', $parametre['idOrder'], PDO::PARAM_INT);
        $prep->execute();
    }
    else{
        $sql = "UPDATE ldb_orders SET status = :status WHERE id_presta_order = :idOrder";
        $prep = $pdo_mojp->prepare($sql);
        $prep->bindValue(':status', $parametre['status'], PDO::PARAM_INT);
        $prep->bindValue(':idOrder', $parametre['idOrder'], PDO::PARAM_INT);
        $prep->execute();
    }
}

function numberIdMojp(){
    global $pdo_mojp;

    $sql = "SELECT id_presta_order FROM ldb_orders";
    return $pdo_mojp->query($sql)->fetchAll();
}

function informationsMOJP($idOrder){
    global $pdo_mojp;

    $sql = "SELECT * FROM ldb_orders WHERE id_presta_order = $idOrder";
    return $pdo_mojp->query($sql)->fetchAll();
}

function endsWith($string, $endString){
    $len = strlen($endString);

    if ($len == 0) {
        return true;
    }
    return (substr($string, -$len) == $endString);
}