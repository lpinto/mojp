<?php
    $delimiter = ",";

    $file = fopen('form-db.txt', 'r');

    while (!feof($file))
    {
        $line = fgets($file);

        $a_data_txt = str_getcsv($line, $delimiter);
    }


    fclose($file);

    ini_set('display_errors', 1);
    define('DB_NAME_MOJP', $a_data_txt[0]);
    define('DB_DSN_MOJP', 'mysql:host=localhost;dbname=' . DB_NAME_MOJP. ';charset=utf8');
    define('DB_USER_MOJP', $a_data_txt[1]);
    define('DB_PASSWORD_MOJP', $a_data_txt[2]);
    define('DEBUG', false);

    define('DB_NAME_PS', $a_data_txt[3]);
    define('DB_DSN_PS', 'mysql:host=localhost;dbname=' . DB_NAME_PS. ';charset=utf8');
    define('DB_USER_PS', $a_data_txt[4] );
    define('DB_PASSWORD_PS', $a_data_txt[5]);

    $dbError = '';

    function connect_mojp()
    {
      global $dbError;
      $opt = array(
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, //ASSOC,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_EMULATE_PREPARES => false
      );
      try {
        return new PDO(DB_DSN_MOJP, DB_USER_MOJP, DB_PASSWORD_MOJP, $opt);
      } catch (PDOException $e) {
        $dbError = 'Oups ! Connexion SGBD impossible !';
        if (DEBUG) :
          $dbError .= "<br/>" . $e->getMessage();
        endif;
      }
    }

    function connect_ps()
    {
        global $dbError;
        $opt = array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, //ASSOC,
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_EMULATE_PREPARES => false
        );
        try {
            return new PDO(DB_DSN_PS, DB_USER_PS, DB_PASSWORD_PS, $opt);
        } catch (PDOException $e) {
            $dbError = 'Oups ! Connexion SGBD impossible !';
            if (DEBUG) :
                $dbError .= "<br/>" . $e->getMessage();
            endif;
        }
    }

    // initialisation de la variable globale $pdo
    $pdo_mojp = connect_mojp();
    $pdo_ps = connect_ps();

    if ($dbError) {
      die('<div class="ui red inverted segment"> <p>'
              . $dbError
              . '</p></div></body></html>');
    }